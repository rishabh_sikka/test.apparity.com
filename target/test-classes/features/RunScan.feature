Feature: Scan Verification

  Scenario Outline: User is able to successfully run a scan
    Given Chrome browser is open successfully
    And User enters URL and SSL is bypassed successfully
    When User enters <username> and <password>
    And Clicks on login button
    Then User is navigated to the homepage
    When User clicks on admin button
    Then User should navigate to Admin Page
    When User clicks on discovery scan button
    Then User should navigate to "Scan Summary" page
    When User clicks on "Start a New Scan" button
    Then User should navigate to "Add a scan" page
    When User should enter a unique scan name
    And User clicks on Continue to Scan settingss button
    Then User should navigate to scan settings page
    
    

    Examples: 
      | username       | password     |
      | "Apparity_admin" | "Apparity@123" |