Feature: User is able to login into Apparity successfully

  Scenario Outline: User is able to successfully login to Apparity
    Given Chrome browser is open successfully
    And User enters URL and SSL is bypassed successfully
    When User enters <Username>
    And User enters <Password>
    And Clicks on login button
    Then User is navigated to the homepage

    Examples:

      | Username       | Password     |
      | "Apparity_admin" | "Apparity@123" |
      |"rishabh"|"Apparity@123"|