$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ApparityLogin.feature");
formatter.feature({
  "name": "User is able to login into Apparity successfully",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Chrome browser is open successfully",
  "keyword": "Given "
});
formatter.step({
  "name": "User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.step({
  "name": "User enters \u003cUsername\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "User enters \u003cPassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Clicks on login button",
  "keyword": "And "
});
formatter.step({
  "name": "User is navigated to the homepage",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Username",
        "Password"
      ]
    },
    {
      "cells": [
        "\"Apparity_admin\"",
        "\"Apparity@123\""
      ]
    },
    {
      "cells": [
        "\"rishabh\"",
        "\"Apparity@123\""
      ]
    }
  ]
});
formatter.scenario({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Chrome browser is open successfully",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.chrome_browser_is_open()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.user_enters_URL_and_SSL_is_bypassed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters \"Apparity_admin\"",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User enters \"Apparity@123\"",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Clicks on login button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.clicks_on_login_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User is navigated to the homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.user_is_navigated_to_the_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Chrome browser is open successfully",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.chrome_browser_is_open()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.user_enters_URL_and_SSL_is_bypassed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters \"rishabh\"",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User enters \"Apparity@123\"",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Clicks on login button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.clicks_on_login_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User is navigated to the homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.user_is_navigated_to_the_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("file:src/test/resources/features/Survey.feature");
formatter.feature({
  "name": "User is able to create a survey",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "User is logged in",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "Chrome browser is open for survey",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.chrome_browser_is_open_for_survey()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters URL",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_enters_URL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters username \"apparity_admin\" and password \"Apparity@123\"",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_enters_username_and_password(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Clicks on login button for survey",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.clicks_on_login_button_for_survey()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is navigated to the homepage for survey",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_is_navigated_to_the_homepage_for_survey()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User is able to successfully create a survey",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User clicks on survey button in the left side bar",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_clicks_on_survey_button_in_the_left_side_bar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is navigated to compliance survey screen",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_is_navigated_to_compliance_survey_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clilck on create new survey button",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_clilck_on_create_new_survey_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should navigate to create new survey screen",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_should_navigate_to_create_new_survey_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters compliance survey name and User fills the description field",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_enters_enters_compliance_survey_name_and_User_fills_the_description_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User selects starts date and end date",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_selects_starts_date_and_end_date()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Apply button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_clicks_on_Apply_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Browse button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ComplianceSurvey.user_clicks_on_Browse_button()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/sample.feature");
formatter.feature({
  "name": "User is able to login into Apparity successfully",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "sample browser is open successfully",
  "keyword": "Given "
});
formatter.step({
  "name": "sample User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.step({
  "name": "User enters username \u003cUsername\u003e and  User enters password \u003cPassword\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "sample Clicks on login button",
  "keyword": "And "
});
formatter.step({
  "name": "sample User is navigated to the homepage",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Username",
        "Password"
      ]
    },
    {
      "cells": [
        "\"Apparity_admin\"",
        "\"Apparity@123\""
      ]
    },
    {
      "cells": [
        "\"rishabh\"",
        "\"Apparity@123\""
      ]
    }
  ]
});
formatter.scenario({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "sample browser is open successfully",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_browser_is_open_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_User_enters_URL_and_SSL_is_bypassed_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters username \"Apparity_admin\" and  User enters password \"Apparity@123\"",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.user_enters_username_and_User_enters_password(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample Clicks on login button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_Clicks_on_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User is navigated to the homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_User_is_navigated_to_the_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "sample browser is open successfully",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_browser_is_open_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_User_enters_URL_and_SSL_is_bypassed_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters username \"rishabh\" and  User enters password \"Apparity@123\"",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.user_enters_username_and_User_enters_password(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample Clicks on login button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_Clicks_on_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User is navigated to the homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.SammpleApparityLogin.sample_User_is_navigated_to_the_homepage()"
});
formatter.result({
  "status": "passed"
});
});