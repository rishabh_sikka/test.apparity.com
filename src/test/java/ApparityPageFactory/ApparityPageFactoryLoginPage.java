package ApparityPageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApparityPageFactoryLoginPage {
	
	@FindBy(id = "inputUsername")
	@CacheLookup
	WebElement txt_username;
	
	@FindBy(id = "inputPassword")
	@CacheLookup
	WebElement txt_password;
	
	@FindBy(xpath = "/html/body/div[2]/form/button")
	@CacheLookup
	WebElement btn_submit;
	
	WebDriver driver;
	
public ApparityPageFactoryLoginPage(WebDriver driver) {
		
		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 50);
		PageFactory.initElements(factory, this);
		
	}

public void enterUsername(String username) {
	txt_username.sendKeys(username);
}

public void enterPassword(String password) {
	txt_password.sendKeys(password);
}
	
public void clickSubmitButton() {
	btn_submit.submit();
}

}
