package ApparityPageFactory;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApparityPageFactoryComplianceSurvey {

	@FindBy(xpath = "//h4[contains(text(),'Compliance Survey')]")
	@CacheLookup
	WebElement linktext_ComplianceSurvey;
	
	@FindBy(xpath = "//button[@class = \"mat-focus-indicator float-right create-button mat-raised-button mat-button-base\"]")
	@CacheLookup
	WebElement btn_CreateNewSurvey;

	WebDriver driver;

	public ApparityPageFactoryComplianceSurvey(WebDriver driver) {
		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 50);
		PageFactory.initElements(factory, this);
	}

	public void switchWindow() {
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		while(iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if(!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
	}

	public void isComplianceSurveyText() {
		linktext_ComplianceSurvey.isDisplayed();
		boolean flag = linktext_ComplianceSurvey.isDisplayed();
		System.out.println(flag);
	}
	
	public void clickCreateNewSurveyButton() {
		btn_CreateNewSurvey.click();
	}

}
