package ApparityPageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApparityPageFactoryHomepage {

	@FindBy(xpath  = "/html/body/div[2]/div/div[1]/div[1]/div[1]/label")
	@CacheLookup
	WebElement txt_InventoryListing;
	
	WebDriver driver;
	
	
	public ApparityPageFactoryHomepage(WebDriver driver) {
		
		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 50);
		PageFactory.initElements(factory, this);
	}
	
	public void isInventoryListingLogo() {
		txt_InventoryListing.isDisplayed();
	}
}
