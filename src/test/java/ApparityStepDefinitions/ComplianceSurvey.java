package ApparityStepDefinitions;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import ApparityPageFactory.ApparityPageFactoryComplianceSurvey;
import ApparityPageFactory.ApparityPageFactoryCreateNewSurvey;
import ApparityPageFactory.ApparityPageFactoryHomepage;
import ApparityPageFactory.ApparityPageFactoryLeftSideBar;
import ApparityPageFactory.ApparityPageFactoryLoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ComplianceSurvey {

	WebDriver driver = null;
	ApparityPageFactoryLoginPage Login;
	ApparityPageFactoryHomepage homepage;
	ApparityPageFactoryLeftSideBar LeftSideBar;
	ApparityPageFactoryComplianceSurvey ComplianceSurvey;
	ApparityPageFactoryCreateNewSurvey CreateNewSurvey;
	private String username;
	private String password;

	@Given("Chrome browser is open for survey")
	public void chrome_browser_is_open_for_survey() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.setAcceptInsecureCerts(true);
		driver = new ChromeDriver(options);
	}

	@And("User enters URL")
	public void user_enters_URL() {
		driver.manage().window().maximize();
		driver.get("https://10.127.130.110/");
	}

	@When("User enters username {string} and password {string}")
	public void user_enters_username_and_password(String username, String password) {
		this.username = username;
		this.password = password;
		Login = new ApparityPageFactoryLoginPage(driver);
		Login.enterUsername(username);
		Login.enterPassword(password);
	}

	@And("Clicks on login button for survey")
	public void clicks_on_login_button_for_survey() {
		Login.clickSubmitButton();
	}

	@Then("User is navigated to the homepage for survey")
	public void user_is_navigated_to_the_homepage_for_survey() {
		homepage = new ApparityPageFactoryHomepage(driver);
		homepage.isInventoryListingLogo();
	}

	@And("User clicks on survey button in the left side bar")
	public void user_clicks_on_survey_button_in_the_left_side_bar() {
		LeftSideBar = new ApparityPageFactoryLeftSideBar(driver);
		LeftSideBar.clickSurveyButton();
	}

	@Then("User is navigated to compliance survey screen")
	public void user_is_navigated_to_compliance_survey_screen() throws InterruptedException {
		ComplianceSurvey = new ApparityPageFactoryComplianceSurvey(driver);
		ComplianceSurvey.switchWindow();
		ComplianceSurvey.isComplianceSurveyText();
	}

	@When("User clilck on create new survey button")
	public void user_clilck_on_create_new_survey_button() {
		ComplianceSurvey.clickCreateNewSurveyButton();
	}

	@Then("User should navigate to create new survey screen")
	public void user_should_navigate_to_create_new_survey_screen() {
		CreateNewSurvey = new ApparityPageFactoryCreateNewSurvey(driver);
		CreateNewSurvey.isCreateNewSurveyLink();
	}
	
	@When("User enters compliance survey name and User fills the description field")
	public void user_enters_enters_compliance_survey_name_and_User_fills_the_description_field() {
		CreateNewSurvey.enterComplianceSurveyName();
		CreateNewSurvey.enterDescription();
	}

	@And("User selects starts date and end date")
	public void user_selects_starts_date_and_end_date() throws InterruptedException {
		CreateNewSurvey.enterStartAndEndDate();
		Thread.sleep(5000);
	}

	@And("User clicks on Apply button")
	public void user_clicks_on_Apply_button() {
	    CreateNewSurvey.clickApplyButton();
	}
	
	@And("User clicks on Browse button")
	public void user_clicks_on_Browse_button() throws InterruptedException, IOException {
	    CreateNewSurvey.clickBrowseButton();
	    
	}

}
