package ApparityStepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import ApparityPageFactory.ApparityPageFactoryHomepage;
import ApparityPageFactory.ApparityPageFactoryLoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SammpleApparityLogin {
	WebDriver driver;
	ApparityPageFactoryLoginPage Login;
	ApparityPageFactoryHomepage homepage;
	
	private String username;
	private String password;
	
	@Given("sample browser is open successfully")
	public void sample_browser_is_open_successfully() {
		WebDriverManager.chromedriver().setup();
	    ChromeOptions options = new ChromeOptions();
	    options.setAcceptInsecureCerts(true);
	    driver = new ChromeDriver(options);
	}

	@And("sample User enters URL and SSL is bypassed successfully")
	public void sample_User_enters_URL_and_SSL_is_bypassed_successfully() {
		driver.get("https://10.127.130.110/");
	}

	@When("User enters username {string} and  User enters password {string}")
	public void user_enters_username_and_User_enters_password(String username, String password) {
	    this.username=username;
	    this.password=password;
	    Login = new ApparityPageFactoryLoginPage(driver);
	    Login.enterUsername(username);
	    Login.enterPassword(password);
	}

	@And("sample Clicks on login button")
	public void sample_Clicks_on_login_button() {
		Login.clickSubmitButton();
	}

	@Then("sample User is navigated to the homepage")
	public void sample_User_is_navigated_to_the_homepage() {
		homepage = new ApparityPageFactoryHomepage(driver);
	    homepage.isInventoryListingLogo();
	    driver.quit();
	}




}
