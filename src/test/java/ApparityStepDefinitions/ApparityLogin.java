package ApparityStepDefinitions;

import java.sql.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import ApparityPageFactory.ApparityPageFactoryHomepage;
import ApparityPageFactory.ApparityPageFactoryLoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ApparityLogin {
	WebDriver driver;
	ApparityPageFactoryLoginPage Login;
	ApparityPageFactoryHomepage homepage;
	
	private String username;
	private String password;
	
	
	@Given("Chrome browser is open successfully")
	public void chrome_browser_is_open() {
		WebDriverManager.chromedriver().setup();
	    ChromeOptions options = new ChromeOptions();
	    options.setAcceptInsecureCerts(true);
	    driver = new ChromeDriver(options);
	}

	@And("User enters URL and SSL is bypassed successfully")
	public void user_enters_URL_and_SSL_is_bypassed() {
	    driver.get("https://10.127.130.110/");
	}

	@When("User enters {string} and {string}")
	public void user_enters_and(String username, String password) {
	    this.username = username;
	    this.password = password;
	    Login = new ApparityPageFactoryLoginPage(driver);
	    Login.enterUsername(username);
	    Login.enterPassword(password);
	    
	}

	@And("Clicks on login button")
	public void clicks_on_login_button() {
	    Login.clickSubmitButton();
	    
	}

	@Then("User is navigated to the homepage")
	public void user_is_navigated_to_the_homepage() {
	    homepage = new ApparityPageFactoryHomepage(driver);
	    homepage.isInventoryListingLogo();
	    driver.quit();
	    		
	}


}
