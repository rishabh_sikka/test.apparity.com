Feature: User is able to create a survey

  Background: User is logged in
    Given Chrome browser is open for survey
    And User enters URL
    When User enters username "apparity_admin" and password "Apparity@123"
    And Clicks on login button for survey
    Then User is navigated to the homepage for survey

  Scenario: User is able to successfully create a survey
    And User clicks on survey button in the left side bar
    Then User is navigated to compliance survey screen
    When User clilck on create new survey button
    Then User should navigate to create new survey screen
		When User enters compliance survey name and User fills the description field
		And User selects starts date and end date
		And User clicks on Apply button
		And User clicks on Browse button